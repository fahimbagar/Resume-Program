﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Security.Principal;

namespace Resume_Process
{
    internal class Program
    {
        #region ProcessExtension

        [Flags]
        public enum ThreadAccess
        {
            TERMINATE = (0x0001),
            SUSPEND_RESUME = (0x0002),
            GET_CONTEXT = (0x0008),
            SET_CONTEXT = (0x0010),
            SET_INFORMATION = (0x0020),
            QUERY_INFORMATION = (0x0040),
            SET_THREAD_TOKEN = (0x0080),
            IMPERSONATE = (0x0100),
            DIRECT_IMPERSONATION = (0x0200)
        }

        [DllImport("kernel32.dll")]
        static extern IntPtr OpenThread(ThreadAccess dwDesiredAccess, bool bInheritHandle, uint dwThreadId);
        [DllImport("kernel32.dll")]
        static extern int ResumeThread(IntPtr hThread);

        public static void ResumeProcess(int pid)
        {
            var process = Process.GetProcessById(pid);

            if (process.ProcessName == string.Empty)
                return;

            foreach (ProcessThread pT in process.Threads)
            {
                IntPtr pOpenThread = OpenThread(ThreadAccess.SUSPEND_RESUME, false, (uint)pT.Id);

                if (pOpenThread == IntPtr.Zero)
                {
                    continue;
                }

                int suspendCount;
                do
                {
                    suspendCount = ResumeThread(pOpenThread);
                } while (suspendCount > 0);
            }
        }

        #endregion

        public static bool IsUserAdministrator()
        {
            bool isAdmin;
            WindowsIdentity user = null;
            try
            {
                user = WindowsIdentity.GetCurrent();
                var principal = new WindowsPrincipal(user);
                isAdmin = principal.IsInRole(WindowsBuiltInRole.Administrator);
            }
            catch (Exception ex)
            {
                isAdmin = false;
                Console.WriteLine($"{ex.Data}\n{ex.Message}\n{ex.Source}");
            }
            finally
            {
                user?.Dispose();
            }
            return isAdmin;
        }

        private static void Main()
        {
            if (!IsUserAdministrator())
            {
                Console.WriteLine("Please run this with admin privilege.");
                Environment.Exit(1);
            }

            var processlist = Process.GetProcesses();

            foreach (var theprocess in processlist)
            {
                try
                {
                    if (theprocess.Threads[0].WaitReason.ToString() != "Suspended") continue;
                    Console.Write(
                        "Process: {0} ID: {1} ",
                        theprocess.ProcessName,
                        theprocess.Id
                    );

                    ResumeProcess(theprocess.Id);

                    Console.WriteLine("  ...resumed...");
                }
                catch (InvalidOperationException)
                {
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"{ex.Data}\n{ex.Message}\n{ex.Source}");
                }
            }
        }
    }
}
